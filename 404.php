<?php get_header(); ?>

	<div class="f_site_banner" id="pageBanner"><div class="c_banner"></div></div><!-- .f_site__banner // this is needed for top menu-->

	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">

					<h2 class="c_ttl-b"><?php _e( 'お探しのページが見つかりませんでした。', '' ); ?></h2>
					
					<div class="c_content__inner">
						このページは一時的にアクセスができない状況か、URLが変更・削除された可能性がございます。
						<br /><a href="<?php echo get_home_url(); ?>">HOME</a>または<a href="<?php echo esc_url( home_url( '/' ) ); ?>sitemap">サイトマップ</a>から目的のページをお探しください。
					</div>
			
				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

<?php get_footer(); ?>