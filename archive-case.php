<?php get_header(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">
					<h2 class="c_ttl-b"><?php post_type_archive_title(); ?></h2>					
					
					<div class="p_list__wide">
						<?php custom_posttype('','case','8');
						if ($w_query->have_posts()) :
							echo '<ul class="p_case__list">';
							while ($w_query->have_posts()) :
								$w_query->the_post(); 
								$be_img = get_field('before_images_before_image_01');
								$af_img = get_field('after_images_after_image_01');
								$du_img = get_template_directory_uri().'/img/case_dummy.jpg'; ?>

								<li>
									<ul class="p_case__img">
										<li><img src="<?php echo ( $be_img ) ? esc_url($be_img['url']) : $du_img; ?>" alt="BEFORE" /><div>BEFORE</div></li>
										<li><img src="<?php echo ( $af_img ) ? esc_url($af_img['url']) : $du_img; ?>" alt="AFTER" /><div>AFTER</div></li>
									</ul>
									<div class="p_case__wrap">
										<h3 class="c_ttl-d"><?php the_title(); ?></h3>
										<div class="p_case__date">投稿日：<?php the_time('Y.m.d'); ?></div>
										<div class="p_pickup__btn mt20">
											<a href="<?php echo get_permalink(); ?>" class="c_btn is_nobd">
												<span class="c_btn__txt">MORE</span>
											</a>
										</div>
									</div>
								</li>

							<?php
							endwhile;
							echo '</ul>';

						posts_pagination();							
						endif;
						wp_reset_postdata(); //クエリのリセット ?>
					</div>

				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

<?php get_footer(); ?>
