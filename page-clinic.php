<?php 

/**
* Template Name: Clinic
*/

if( $post->post_parent == 0 ) {
	wp_redirect( home_url().'/clinic/about/' ); exit;
}

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>
	
	<div id="main_area" class="f_site_main">
		<main>			
			<?php the_content(); ?>
			<?php get_template_part( 'inc/contact' ); ?>
		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
