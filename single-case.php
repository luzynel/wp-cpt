<?php 

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>
	
	<div id="main_area" class="f_site_main">
		<main>			

			<section class="f_innerpage">
				<div class="l_wrapper">
					<h2 class="c_ttl-b"><?php the_title(); ?></h2>
					<ul class="p_case__list">
						<?php
							$be_img1 = get_field('before_images_before_image_01');
							$af_img1 = get_field('after_images_after_image_01');
							$be_img2 = get_field('before_images_before_image_02');
							$af_img2 = get_field('after_images_after_image_02');
							$be_img3 = get_field('before_images_before_image_03');
							$af_img3 = get_field('after_images_after_image_03');
							$du_img = get_template_directory_uri().'/img/case_dummy.jpg';
							$pr = get_field('price');
							$tr = get_field('trouble');
							$do = get_field('downtime_risk');
						?>
						<li>
							<ul class="p_case__img is_single">
								<li><img src="<?php echo ( $be_img1 ) ? esc_url($be_img1['url']) : $du_img; ?>" alt="BEFORE" /><div>BEFORE</div></li>
								<li><img src="<?php echo ( $af_img1 ) ? esc_url($af_img1['url']) : $du_img; ?>" alt="AFTER" /><div>AFTER</div></li>
								<li><img src="<?php echo ( $be_img2 ) ? esc_url($be_img2['url']) : $du_img; ?>" alt="BEFORE" /><div>BEFORE</div></li>
								<li><img src="<?php echo ( $af_img2 ) ? esc_url($af_img2['url']) : $du_img; ?>" alt="AFTER" /><div>AFTER</div></li>
								<li><img src="<?php echo ( $be_img3 ) ? esc_url($be_img3['url']) : $du_img; ?>" alt="BEFORE" /><div>BEFORE</div></li>
								<li><img src="<?php echo ( $af_img3 ) ? esc_url($af_img3['url']) : $du_img; ?>" alt="AFTER" /><div>AFTER</div></li>
							</ul>
							<div class="p_case__wrap is_single">
								<div class="p_case__date">投稿日：<?php the_time('Y.m.d'); ?></div>
								<table class="p_case__tbl">
									<tr>
										<th>費用</th>
										<td><?php echo esc_html( $pr ); ?></td>
									</tr>
									<tr>
										<th>施術内容</th>
										<td><?php echo esc_html( $tr ); ?></td>
									</tr>
									<tr>
										<th>ダウンタイム・リスク</th>
										<td><?php echo esc_html( $do ); ?></td>
									</tr>
								</table>
								<div>
									<?php the_content(); ?>
								</div>
							</div>
						</li>
					</ul>
					<div class="mt80 c_txt-c">
						<a class="c_btn" href="<?php echo get_home_url(); ?>/case"><span class="c_btn__txt">症例一覧に戻る</span></a>
					</div>
				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>
		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
