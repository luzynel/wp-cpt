<?php get_header(); ?>

<div class="f_site_banner" id="pageBanner">
    <div class="f_site_banner__wrapper">      
        <ul class="fade c_banner">
          <li><img class="c_banner__img pc-only" src="<?php echo get_template_directory_uri(); ?>/img/bnr_img01.jpg" alt="心に寄り添い悩みに向き合う" /><img class="c_banner__img sp-only" src="<?php echo get_template_directory_uri(); ?>/img/bnr_img01_sp.jpg" alt="心に寄り添い悩みに向き合う" /></li>
          <li><img class="c_banner__img pc-only" src="<?php echo get_template_directory_uri(); ?>/img/bnr_img01.jpg" alt="心に寄り添い悩みに向き合う" /><img class="c_banner__img sp-only" src="<?php echo get_template_directory_uri(); ?>/img/bnr_img01_sp.jpg" alt="心に寄り添い悩みに向き合う" /></li>
          <li><img class="c_banner__img pc-only" src="<?php echo get_template_directory_uri(); ?>/img/bnr_img01.jpg" alt="心に寄り添い悩みに向き合う" /><img class="c_banner__img sp-only" src="<?php echo get_template_directory_uri(); ?>/img/bnr_img01_sp.jpg" alt="心に寄り添い悩みに向き合う" /></li>
        </ul>        
    </div>
  </div><!-- .f_site__banner -->
  <div class="c_banner__txt sp-only">『スキンコスメクリニック』では、できる限り“切らない美容医療”で“あなたらしい自然な美しさ”を引き出すエイジングケア治療を行っております。</div>

  <div id="main_area" class="f_site_main">
  	<main>
  	  <section class="f_sec01">
        <div class="l_wrapper">          
          <div class="p_adbox">
            <a class="c_btn is_ad" href="">
              <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/img/ad_img.jpg" alt="RECOMMEND 季節に合わせたメニューやトライアルプランをご紹介" />
              <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/img/ad_img_sp.jpg" alt="RECOMMEND 季節に合わせたメニューやトライアルプランをご紹介" />
              <div class="c_btn__txt">
                <span>RECOMMEND</span> <small class="pc-only">季節に合わせたメニューやトライアルプランをご紹介</small><br class="pc-only">
                今月のおすすめプラン
              </div>
            </a>
            <p class="sp-only">季節に合わせたメニューやトライアルプランをご紹介</p>
          </div>
        </div>
  	  </section><!-- .f_sec01 -->

      <section class="f_section f_sec02">
        <div class="l_wrapper">   
          <img class="f_section-img" src="<?php echo get_template_directory_uri(); ?>/img/concept-img.jpg" alt="CONCEPT 自然な仕上がりの美しさ" />
          <div class="f_section__inner">
            <h2 class="c_ttl is_large">
              <span>CONCEPT</span>自然な仕上がりの<br>美しさ
            </h2>
            <div class="f_section__txt">   
              <p>私たちの使命は、「その人に合った美しさと魅力を引き出す」ことです。施術後に誰の目から見ても明らかなほどの変化は、満足度が高いとしても不自然と言わざるをえません。あくまでも自然な仕上がりで若さをより長くキープすることが、私たちが考えるエイジングケア治療です。</p>         
              <div class="c_btn-wrapper">
                <a class="c_btn" href="">無料カウンセリング予約</a><br class="pc-only">
                <a class="c_btn" href="">無料メール相談</a>
              </div>
            </div>
          </div>
        </div>
  	  </section><!-- .f_sec02 -->

      <section class="f_section f_sec03">
        <div class="l_wrapper">
          <img class="f_section-img01" src="<?php echo get_template_directory_uri(); ?>/img/policy-img01.jpg" alt="POLICY 安心価格とプロの技術を提供" />
          <div class="f_section__inner">
            <h2 class="c_ttl">
              <span>POLICY</span>続けやすい、通いやすい<br>地域に寄り添う美容医療を
            </h2>
            <div class="f_section__txt">   
              <p>『スキンコスメクリニック』は、それまで敷居の高かった美容医療をもっと身近にしたいという思いから、2005年秋、「川崎」に誕生しました。その後、駅近の通いやすいクリニックを基本方針に戸塚・二子玉川の3院を展開しております。これからも、信頼される良質な美容医療を提供し、安心して気軽に通っていただけるクリニックを目指してまいります。</p>         
              <div class="c_btn-wrapper">
                <a class="c_btn" href="">クリニック紹介</a><br class="pc-only">
                <a class="c_btn" href="">初めての方へ</a>
              </div>
            </div>
          </div>
          <img class="f_section-img02" src="<?php echo get_template_directory_uri(); ?>/img/policy-img02.jpg" alt="POLICY 安心価格とプロの技術を提供" />
        </div>
  	  </section><!-- .f_sec03 -->

      <section class="f_sec04">
        <h2 class="c_ttl is_center">
          <span>PICK UP</span>おすすめの施術を<br class="sp-only">ご紹介します
        </h2>   
        <div class="p_pickup">
          <div class="p_pickup__slick">
            <div>
              <div class="p_pickup__item p_pickup__slide">
                <img class="p_pickup__media" src="<?php echo get_template_directory_uri(); ?>/img/pickup-img-01.jpg" alt="シミ治療" />
                <div class="p_pickup__txt">
                  <div class="p_pickup__ttl">シミ治療</div>
                  <p>肝斑・シミ・くすみ・そばかす・老人性イボ・色素沈着などのお悩みに</p>
                  <div class="p_pickup__btn"><a class="c_btn is_nobd" href="#" >MORE</a></div>
                </div>
              </div> 
            </div>
            <div>
              <div class="p_pickup__item p_pickup__slide">
                <img class="p_pickup__media" src="<?php echo get_template_directory_uri(); ?>/img/pickup-img-02.jpg" alt="メスを使わない若返り" />
                <div class="p_pickup__txt">
                  <div class="p_pickup__ttl">メスを使わない若返り</div>
                  <p>肝斑・シミ・くすみ・そばかす・老人性イボ・色素沈着などのお悩みに</p>
                  <div class="p_pickup__btn"><a class="c_btn is_nobd" href="#" >MORE</a></div>
                </div>
              </div> 
            </div>
            <div>
              <div class="p_pickup__item p_pickup__slide">
                <img class="p_pickup__media" src="<?php echo get_template_directory_uri(); ?>/img/pickup-img-03.jpg" alt="メスを使わない若返り" />
                <div class="p_pickup__txt">
                  <div class="p_pickup__ttl">メスを使わない若返り</div>
                  <p>肝斑・シミ・くすみ・そばかす・老人性イボ・色素沈着などのお悩みに</p>
                  <div class="p_pickup__btn"><a class="c_btn is_nobd" href="#" >MORE</a></div>
                </div>
              </div> 
            </div>           
          </div>
        </div>
  	  </section><!-- .f_sec04 -->

      <section class="f_troubles">
        <div class="l_wrapper">
          <div class="p_troubles">
            <h2 class="c_ttl p_troubles__ttl">
              <div><span>TROUBLES</span>お悩みから施術を<br class="sp-only">お選びください</div>
            </h2>
            <div id="tabs" class="p_troubles__content">
              <ul class="p_troubles__tabs">
                <li><a href="#tabs_skin">SKIN<span>お肌のお悩み</span></a></li>
                <li><a href="#tabs_face">FACE<span>お顔のお悩み</span></a></li>
                <li><a href="#tabs_body">BODY<span>身体のお悩み</span></a></li>
                <li><a href="#tabs_other">OTHER<span>その他</span></a></li>
              </ul>
              <div class="p_troubles__menus">
                <ul id="tabs_skin" class="p_troubles__menu">
                  <li><a href="#">シミ・肝斑</a></li>
                  <li><a href="#">シワ</a></li>
                  <li><a href="#">イボ・ホクロ</a></li>
                  <li><a href="#">毛穴・黒ずみ</a></li>
                  <li><a href="#">美白・くすみ</a></li>
                  <li><a href="#">ニキビ・ニキビ跡</a></li>
                </ul>
                <ul id="tabs_face" class="p_troubles__menu">
                  <li><a href="#">たるみ・リフトアップ</a></li>
                  <li><a href="#">くぼみ・こけ</a></li>
                  <li><a href="#">二重まぶた・目元形成</a></li>
                  <li><a href="#">鼻</a></li>
                  <li><a href="#">唇・口元</a></li>
                  <li><a href="#">小顔</a></li>
                </ul>
                <ul id="tabs_body" class="p_troubles__menu">
                  <li><a href="#">医療脱毛</a></li>
                  <li><a href="#">ワキが・多汗症</a></li>
                  <li><a href="#">部分痩せ・痩身</a></li>
                  <li><a href="#">蒙古性苔癬</a></li>
                </ul>
                <ul id="tabs_other" class="p_troubles__menu">
                  <li><a href="#">注射・点滴</a></li>
                  <li><a href="#">毛髪再生</a></li>
                  <li><a href="#">医療アートメイク</a></li>
                  <li><a href="#">ピアス</a></li>
                </ul>
              </div>
            </div><!-- .p_troubles__content -->
            <a class="c_btn" href="javascript:;">診療内容</a>
          </div>
        </div><!-- .l_wrapper -->
      </section><!-- .f_troubles -->

      <section class="f_news">
        <div class="l_wrapper">
          <div class="p_news">
            <div class="p_news__info">
              <h2 class="c_ttl p_news__ttl">
                <span>NEWS &amp; TOPICS</span>最新情報
              </h2>
              <p class="p_news__txt">スキンコスメクリニックからの<br class="pc-only">最新のお知らせです。</p>
            </div>
            <div class="p_news__list">
              <?php
                $a_args = array(
                  'post_type' => 'post',
                  'order' => 'DESC',
                  'posts_per_page' => 6,
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'category',
                      'terms' => 'news',
                      'field' => 'slug'
                    )
                  )
                );
                $a_posts = get_posts( $a_args );

                if ($a_posts):
                  echo '<ul>';
                  foreach( $a_posts as $o_post ) {
                    $pt_default = get_the_title($o_post->ID);
                    $pl_title = get_field( "page_link_title", $o_post->ID );
                    $cus_pl = get_field( "custom_page_link", $o_post->ID );
                    $pl = get_field( "page_link", $o_post->ID );
                    $no_l = get_field( "no_link", $o_post->ID );
                    $permalink = get_permalink( $o_post->ID );
                    $target = 'target="_blank"';

                    if( $pl_title != '' ) {
                      $pt = $pl_title;
                    }else {
                      $pt = $pt_default;
                    }

                    if( $cus_pl != '' ) {
                      $url = $cus_pl;
                      $tar = $target;
                    } elseif( $cus_pl == '' AND $pl == '') {
                      $url = $permalink;
                      $tar = '';
                    } else {
                      $url = $pl;
                      $tar = '';
                    }
                    ?>

                    <?php if( $no_l == true ) { ?>		
                    <li class="is_non">
                      <span class="p_news__date"><?php echo get_the_date('Y.m.d',$o_post->ID); ?></span>
                      <?php echo $pt; ?>
                    </li>        

                    <?php } else { ?>
                    <li>
                      <span class="p_news__date"><?php echo get_the_date('Y.m.d',$o_post->ID); ?></span>
                      <a href="<?php echo $url; ?>" <?php echo $tar; ?>><?php echo $pt; ?></a>                      
                    </li>
                    <?php } ?>

                    <?php 
                  }                    
                  echo '</ul>';
                endif;              
              ?>
              <a href="<?php echo get_home_url(); ?>/news" class="c_btn is_nobd">
                <span class="c_btn__txt">BACK NUMBER</span>
              </a>
            </div>
            <div class="p_news__bnr sp-only">
              <a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/bnr_staffblog.jpg" alt="STAFF BLOG スタッフの美容施術体験日記" /></a>
            </div>
            <ul class="p_news__social">
              <li><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-instagram.svg" alt="instagram" /></a></li>
              <li><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-twitter.svg" alt="twitter" /></a></li>
              <li><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-line.svg" alt="line" /></a></li>
            </ul>

          </div><!-- .p_news -->
        </div><!-- .l_wrapper -->
      </section><!-- .f_news -->
      
      <section class="f_access">
        <div class="p_access">
          <div class="p_access__upper">
            <div class="l_wrapper">
              <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/img/p_access__upper-img.jpg" alt="Access アクセス" />
              <div class="p_access__info">
                <h2 class="c_ttl p_access__ttl"><span>ACCESS</span>アクセス</h2>
                <p>
                  川崎駅前院、戸塚駅前院、二子玉川院の
                  <br class="pc-only">3か所を拠点として美容クリニックを運営
                  <br class="pc-only">しております。
                </p>
              </div>
            </div><!-- .l_wrapper -->
          </div><!-- .p_access__upper -->
        </div>

        <div class="p_clinics">
          <div class="l_wrapper">
            <div class="p_clinic">

              <div class="p_clinic__item">
                <div class="p_clinic__ttl">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/icon-logo.svg" alt="icon" />
                  川崎駅前<span>スキンコスメクリニック</span>
                </div>
                <div class="p_clinic__contents">
                  <div class="p_clinic__subttl">KAWASAKI SKIN COSME CLINIC</div>
                  <div class="p_clinic__tel">
                    <span><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="icon" />0120-63-4112</span>
                  </div>
                  <p class="p_clinic__time">診療時間：10：00～19：00　不定休</p>
                  <div class="p_clinic__txt">
                    <p class="p_clinic__address">
                      〒210-0007
                      <br class="pc-only" />神奈川県川崎市川崎区駅前本町11-1
                      <br class="pc-only" />パシフィックマークス川崎４Ｆ
                      <br class="pc-only" />（旧イーストワンビル）
                    </p>
                    <p class="p_clinic__link"><a href="javascript:;">Google map</a></p>
                  </div><!-- .p_clinic__txt -->
                </div><!-- .p_clinic__contents -->
                <div class="p_clinic__btn">
                  <a class="c_btn is_small" href="javascript:;">
                    <span class="c_btn__txt">川崎駅前院詳細</span>
                  </a>
                </div><!-- .p_clinic__btn -->
              </div><!-- .p_clinic__item -->

              <div class="p_clinic__item">
                <div class="p_clinic__ttl">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/icon-logo.svg" alt="icon" />
                  戸塚駅前<span>スキンコスメクリニック</span>
                </div>
                <div class="p_clinic__contents">
                  <div class="p_clinic__subttl">TOTSUKA SKIN COSME CLINIC</div>
                  <div class="p_clinic__tel">
                    <span><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="icon" />0120-28-4112</span>
                  </div>
                  <p class="p_clinic__time">診療時間：10：00～19：00　不定休</p>
                  <div class="p_clinic__txt">
                    <p class="p_clinic__address">
                      〒244-0003
                      <br class="pc-only" />横浜市戸塚区戸塚町16－6
                      <br class="pc-only" />中村ビル４F
                    </p>
                    <p class="p_clinic__link"><a href="javascript:;">Google map</a></p>
                  </div><!-- .p_clinic__txt -->
                </div><!-- .p_clinic__contents -->
                <div class="p_clinic__btn">
                  <a class="c_btn is_small" href="javascript:;">
                    <span class="c_btn__txt">戸塚駅前院詳細</span>
                  </a>
                </div><!-- .p_clinic__btn -->
              </div><!-- .p_clinic__item -->

              <div class="p_clinic__item">
                <div class="p_clinic__ttl">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/icon-logo.svg" alt="icon" />
                  二子玉川<span>スキンコスメクリニック</span>
                </div>
                <div class="p_clinic__contents">
                  <div class="p_clinic__subttl">FUTAKOTAMAGAWA SKIN COSME CLINIC</div>
                  <div class="p_clinic__tel">
                    <span><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="icon" />0120-71-4112</span>
                  </div>
                  <p class="p_clinic__time">診療時間：10：00～19：00　不定休</p>
                  <div class="p_clinic__txt">
                    <p class="p_clinic__address">
                      〒158－0094
                      <br class="pc-only" />東京都世田谷区玉川3－10－10
                      <br class="pc-only" />フェリトイアB1F
                    </p>
                    <p class="p_clinic__link"><a href="javascript:;">Google map</a></p>
                  </div>
                </div><!-- .p_clinic__contents -->
                <div class="p_clinic__btn">
                  <a class="c_btn is_small" href="javascript:;">
                    <span class="c_btn__txt">二子玉川院詳細</span>
                  </a>
                </div><!-- .p_clinic__btn -->
              </div><!-- .p_clinic__item -->

            </div><!-- .p_clinic__items -->
          </div><!-- .l_wrapper -->
        </div><!-- .p_clinic -->
      </section><!-- .f_access -->

  	  <section class="f_recruit">
        <div class="p_recruit">
          <div class="p_recruit__item">
            <div class="p_recruit--media pc-only"><img src="<?php echo get_template_directory_uri(); ?>/img/p_recruit__media-img.png" alt="採用情報" /></div>
            <div class="p_recruit--media sp-only"><img src="<?php echo get_template_directory_uri(); ?>/img/p_recruit__media-img_sp.jpg" alt="採用情報" /></div>
            <div class="p_recruit__wrapper">
              <div class="p_recruit__content">
                <h2 class="c_ttl p_recruit__ttl">
                  <div><span>RECRUIT</span>採用情報</div>
                </h2>
                <p class="p_recruit__txt">地域に根ざした高度美容医療を提供する同グループ院にご勤務いただける医師を募集しております。</p>
                <a class="c_btn" href="javascript:;">
                  <span class="c_btn__txt">採用情報</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section><!-- .f_recruit -->

  	</main>
  </div>


<?php get_footer(); ?>
