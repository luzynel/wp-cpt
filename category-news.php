<?php get_header(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">
					<h2 class="c_ttl-b">NEWS & TOPICS 最新情報</h2>					
					
					<div class="p_list__wide">
						<?php custom_posttype('news','post','10');
						if ($w_query->have_posts()) :
							echo '<ul>';
							while ($w_query->have_posts()) :
								$w_query->the_post();
								$pt_default = get_the_title();
								$pl_title = get_field( "page_link_title" );
								$cus_pl = get_field( "custom_page_link" );
								$pl = get_field( "page_link" );
								$no_l = get_field( "no_link" );
								$permalink = get_permalink();
								$target = 'target="_blank"';

								if( $pl_title != '' ) {
									$pt = $pl_title;
								}else {
									$pt = $pt_default;
								}

								if( $cus_pl != '' ) {
									$url = $cus_pl;
									$tar = $target;
								} elseif( $cus_pl == '' AND $pl == '') {
									$url = $permalink;
									$tar = '';
								} else {
									$url = $pl;
									$tar = '';
								} ?>

								<li>
									<div class="c_ttl-d is_wdate">
										<span><?php the_time('Y.m.d'); ?></span>
										<h3><?php echo $pt; ?></h3>
									</div>
									<div>
										<?php 
											$c_exc = preg_replace(" ([.*?])",'',get_the_content());
											$c_exc = strip_shortcodes($c_exc);
											$c_exc = strip_tags($c_exc);
											$c_exc = mb_substr($c_exc, 0, 90);
											$c_exc = trim(preg_replace( '/\s+/', ' ', $c_exc));
											//$c_exc = $c_exc.'…';
											echo $c_exc;
										?>
									</div>
									<?php if( $no_l != true ) { ?>
										<div class="p_pickup__btn mt20">
											<a href="<?php echo $url; ?>" <?php echo $tar; ?> class="c_btn is_nobd">
												<span class="c_btn__txt">MORE</span>
											</a>
										</div>
									<?php } ?>
								</li>

							<?php
							endwhile;
							echo '</ul>';

						posts_pagination();
						endif; 
						wp_reset_postdata(); //クエリのリセット ?>
					</div>

				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

<?php get_footer(); ?>
