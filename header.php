<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-control" content="public">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Serif+JP:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
	<link href="https://unpkg.com/swiper/swiper-bundle.min.css">

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick-theme.css" type="text/css" media="all">

  <?php if(is_page('yoyaku') || is_page('sodan')){ 
    echo '<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">';
  } ?>

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/common_header.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/common_footer.css">

<?php wp_head(); ?>

</head>

<body id="btop" <?php body_class(); ?>>

  <!-- Fixed Buttons -->
  <div class="f_site__fixed clearfix">
    <div class="c_btn-fixed pc-only">
      <div class="is_blue">
        <a href="<?php echo get_home_url(); ?>/yoyaku"><img src="<?php echo get_template_directory_uri(); ?>/img/fixed-btn01.svg" alt="Button 01"></a>
        <a href="<?php echo get_home_url(); ?>/sodan"><img src="<?php echo get_template_directory_uri(); ?>/img/fixed-btn02.svg" alt="Button 02"></a>
      </div>
      <a class="is_pagetop" href="#btop"><img src="<?php echo get_template_directory_uri(); ?>/img/fixed-btn03.svg" alt="PAGE TOP"></a>
    </div>
    <div class="c_btn-fixed sp-only">
      <div class="is_gray">
        <ul>
          <li><a href="tel:0120634112"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="telephone">川崎駅前院</a></li>
          <li><a href="tel:0120284112"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="telephone">戸塚駅前院</a></li>
          <li><a href="tel:0120714112"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="telephone">二子玉川院</a></li>
        </ul>
        <span>電話ご予約受付　10：00～19：00　不定休</span>
      </div>
      <ul class="is_blue">
        <li><a href="<?php echo get_home_url(); ?>/yoyaku"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-calendar.svg" alt="calendar icon">オンライン予約</a></li>
        <li><a href="<?php echo get_home_url(); ?>/sodan"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-mail.svg" alt="mail icon">無料メール相談</a></li>
        <li><a class="is_blue__ph"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel-white.svg" alt="telephone">電話</a></li>
      </ul>
      <a class="is_pagetop" href="#btop"><img src="<?php echo get_template_directory_uri(); ?>/img/fixed-btn03_sp.svg" alt="PAGE TOP"></a>
    </div>
  </div> 

  <!-- Header -->
  <div class="f_site__header clearfix">
    <header class="p_header inactive">
      <h1 class="p_header__logo">
        <a href="<?php echo get_home_url(); ?>">
          <img class="is_top" src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Skin Cosme Clinic">
          <img class="is_scroll" src="<?php echo get_template_directory_uri(); ?>/img/logo2.svg" alt="Skin Cosme Clinic">
        </a>
      </h1>

      <div class="p_header__nav">
        <nav class="p_header__nav__list">
          <ul class="menu">
            <li><a href="<?php echo get_home_url(); ?>/menu">診療内容<span>Menu</span></a></li>
            <li><a href="<?php echo get_home_url(); ?>/case">症例一覧<span>Cases</span></a></li>
            <li class="has_dp">
              <span class="is_arrow sp-only"></span>
              <a class="is_nolink">クリニック紹介<span>Clinic</span></a>
              <div class="p_dropdown">
								<ul class="p_dropdown__list">
									<li><a href="<?php echo get_home_url(); ?>/clinic/about">当院について</a></li>
									<li><a href="<?php echo get_home_url(); ?>/clinic/first">初めての方へ</a></li>
                  <li><a href="<?php echo get_home_url(); ?>/clinic/doctor">ドクター紹介</a></li>
									<li><a href="<?php echo get_home_url(); ?>/clinic/kawasaki">川崎駅前院</a></li>
                  <li><a href="<?php echo get_home_url(); ?>/clinic/totsuka">戸塚駅前院</a></li>
                  <li><a href="<?php echo get_home_url(); ?>/clinic/futakotamagawa">二子玉川院</a></li>
                  <li><a href="<?php echo get_home_url(); ?>/clinic/recruit">採用情報</a></li>				
								</ul>
							</div>
            </li>
            <li class="has_dp">
              <span class="is_arrow sp-only"></span>
              <a class="is_nolink">料金・お支払<span>Price</span></a>
              <div class="p_dropdown">
								<ul class="p_dropdown__list">
									<li><a href="<?php echo get_home_url(); ?>/price/list">料金</a></li>
									<li><a href="<?php echo get_home_url(); ?>/price/payment">お支払いについて</a></li>			
								</ul>
							</div>
            </li>
            <li><a href="<?php echo get_home_url(); ?>/column">コラム<span>Column</span></a></li>
          </ul>
        </nav>
        <ul class="p_header__contact sp-only">
          <li><a href="<?php echo get_home_url(); ?>/yoyaku"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-calendar.svg" alt="calendar icon">無料カウンセリング予約</a></li>
          <li><a href="<?php echo get_home_url(); ?>/sodan"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-mail.svg" alt="mail icon">無料メール相談</a></li>
        </ul>
        <ul class="p_header__social sp-only">
          <li><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-instagram.svg" alt="instagram"></a></li>
          <li><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-twitter.svg" alt="twitter"></a></li>
          <li><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-line.svg" alt="line"></a></li>
        </ul>
      </div>

      <div class="p_header__icons sp-only">
				<div class="is_toggle is_navOp">
          <div>
            <span></span><span></span><span></span>
          </div>
          <small class="op">MENU</small>
        </div>
      </div>

    </header>
  </div><!-- .f_site__header -->