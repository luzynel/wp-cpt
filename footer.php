<!-- Start Footer -->
<footer class="f_site__footer">
  <div class="p_footer-top">
    <div class="l_wrapper">
      <div class="p_footer__menu">
        <ul>
          <li><a href="<?php echo get_home_url(); ?>/clinic/kawasaki">川崎駅前院</a></li>
          <li><a href="<?php echo get_home_url(); ?>/clinic/totsuka">戸塚駅前院</a></li>
          <li><a href="<?php echo get_home_url(); ?>/clinic/futakotamagawa">二子玉川院</a></li>
          <li><a href="<?php echo get_home_url(); ?>/privacy-policy">個人情報の取り扱い</a></li>
          <li><a href="<?php echo get_home_url(); ?>/terms-of-service">ご利用規約</a></li>
          <li><a href="<?php echo get_home_url(); ?>/sitemap">サイトマップ</a></li>
        </ul>
      </div>

      <div class="p_footer-btm">
        <div class="p_footer__logo">
          <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.svg" alt="SKINCOSMECLINIC" /></a>
        </div>
        <div class="copyright">&copy;2021 SKINCOSMECLINIC. All Rights Reserved.</div>
      </div>
    </div>
  </div>
</footer>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.6.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-custom-v2.7.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/smooth-scroll.min.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/accordion.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>

<?php wp_footer(); ?>
</body>
</html>
