<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">				
					<div class="c_content__inner">
						<p>美容整形に興味はあっても病院に行くのは不安がある、という方はたくさんいらっしゃいます。どんなことでもお気軽にご相談ください。ご一緒に問題点を解決して、あなたの美しさを引き出すお手伝いをいたします。</p>
						<p>※ご返事に、数日お時間をいただく場合がございます。</p>
						<p>お急ぎの方は、お手数ですが各院までご連絡ください。</p>

						<div class="p_faq__box p_contact__box">
              <div class="p_faq__btns">
                <a class="c_btn" href="/yoyaku"><span class="c_btn__txt">無料カウンセリング予約</span></a>
              </div>
              <ul class="p_contact__inquiries p_faq__contact">
                <li><a href="tel:0120634112">
                    <div class="p_contact__name">川崎駅前院</div>
                    <div class="p_contact__tel"><span>0120-63-4112</span></div>
                    <div class="p_contact__time">（診療時間：10：00～19：00　不定休）</div>
                </a></li>
                <li><a href="tel:0120284112">
                    <div class="p_contact__name">戸塚駅前院</div>
                    <div class="p_contact__tel"><span>0120-28-4112</span></div>
                    <div class="p_contact__time">（診療時間：10：00～19：00　不定休）</div>
                </a></li>
                <li><a href="tel:0120714112">
                    <div class="p_contact__name">二子玉川院</div>
                    <div class="p_contact__tel"><span>0120-71-4112</span></div>
                    <div class="p_contact__time">（診療時間：10：00～19：00　不定休）</div>
                </a></li>
              </ul>
							<ul class="p_contact__inquiries p_faq__contact is_justify-c">
								<li><a href="tel:0442234119">
                    <div class="p_contact__name">各院共通</div>
                    <div class="p_contact__tel"><span>044-223-4119</span></div>
                    <div class="p_contact__time">（診療時間：10：00～19：00　不定休）</div>
                </a></li>
              </ul>
              <div class="p_contact__time sp-only">診療時間：10：00～19：00　不定休</div>
            </div>
					</div>
				</div>
			</section>

			<section class="f_section2 is_white">
				<div class="l_wrapper">
					<h2 class="c_ttl-b">無料メール相談</h2>
					<div class="p_form">
						<h3 class="c_ttl-c"><span>基本情報</span></h3>
						<div class="p_form__inner">
							<form>
								<div class="p_form__content">
									<p><small><span class="is_red">※</span> 印は入力必須項目です。</small></p>
								</div>
								<table class="p_form__tbl">
									<tr>
										<th>氏名 <span class="is_red">※</span></th>
										<td>
											<input type="text" name="name" class="is_wide" size="60" value="">
										</td>
									</tr>
									<tr>
										<th>E-Mailアドレス <span class="is_red">※</span></th>
										<td>
											<input type="text" name="email" class="is_wide" size="60" value="">
										</td>
									</tr>
									<tr>
										<th>E-Mailアドレス［確認用］ <span class="is_red">※</span></th>
										<td>
											<input type="text" name="confirm_email" class="is_wide" size="60" value="">
										</td>
									</tr>
									<tr>
										<th>その他ご要望がありまし<br>たらご記入ください</th>
										<td>
											<textarea name="inquiry" class="is_wide is_widearea" cols="50" rows="5"></textarea>
										</td>
									</tr>
									<tr>
										<th></th>
										<td>
											<div class="c_btn is_formsubmit"><input type="submit" name="submitConfirm" value="入力内容の確認"></div>
										</td>
									</tr>
								</table>
							</form>
						</div>
					</div>
				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
