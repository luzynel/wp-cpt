<?php get_header(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">
					<h2 class="c_ttl-b"><?php post_type_archive_title(); ?> Menu</h2>					
					
					<div class="p_news__list" style="width: 100%;">					
						<?php custom_posttype('','menu','');
						if ($w_query->have_posts()) :
							echo '<ul>';
							while ($w_query->have_posts()) :
								$w_query->the_post();
								$pt_default = get_the_title();
								$permalink = get_permalink(); ?>

								<li>
									<span class="p_news__date"><?php the_time('Y.m.d'); ?></span>
									<a class="is_pttl" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>                      
								</li>
							
							<?php
							endwhile;
							echo '</ul>';

						posts_pagination();							
						endif;
						wp_reset_postdata(); //クエリのリセット ?>						
					</div>

				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

<?php get_footer(); ?>
