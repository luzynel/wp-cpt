<?php 

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<?php
		$a = get_the_content();
		$b = array_filter(get_field('case_section'));		
		$c = get_field('flow_section', false, false);
		$d = get_field('faq_section', false, false);
		$e = get_field('charges_section', false, false);
		$f = get_field('point_content', false, false);
		$g = get_field('trouble_list', false, false);
		
	?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>
	
	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">          
					<div class="p_menu">
						<?php if ($a || $b || $c || $d || $e): ?>
						<ul class="p_menu__list">
							<?php if ($a): ?><li><a href="#feature">特徴・効果</a></li><?php endif; ?>
							<?php if ($b): ?><li><a href="#case">症例</a></li><?php endif; ?>
							<?php if ($c): ?><li><a href="#flow">施術の流れ</a></li><?php endif; ?>
							<?php if ($d): ?><li><a href="#faq">よくある質問</a></li><?php endif; ?>
							<?php if ($e): ?><li><a href="#charges">主な料金</a></li><?php endif; ?>
						</ul>
						<?php endif; ?>

						<div class="c_banner-b__detail_sp">
							<div class="c_banner-b__ttl">
								<?php the_field('sub_title'); ?>   
							</div>
							<p><?php the_field('prologue'); ?></p>            
						</div>

						<?php if ($g): ?>
						<div class="p_menu__box">
							<div class="p_menu__box__ttl">このような悩みの方に</div>
							<div class="p_menu__box__list">
								<?php the_field('trouble_list'); ?>
							</div>
						</div>
						<?php endif; ?>
					</div>

					<?php if ($a): ?>
					<a class="f_anchor" id="feature"></a>
					<div class="c_content">
						<div class="l_wrapper">
							<h2 class="c_ttl-b">「<span class="is_pttl"><?php the_title(); ?></span>」の<br class="sp-only">特徴・効果</h2>							
							<?php the_content('',false); ?>							
						</div>
					</div>
					<?php endif; ?>
				</div>
			</section>

			<?php if ($f): ?>
			<section class="f_point">
				<div class="l_wrapper">
					<div class="p_point">
						<div class="c_ttl is_center p_point__ttl"><span>POINT</span>スキンコスメクリニックのこだわりポイント</div>
						<?php the_field('point_content'); ?>
					</div>
				</div>
			</section>
			<?php endif; ?>

			<?php if ($b): ?>
			<a class="f_anchor" id="case"></a>
			<section class="f_case">
				<div class="l_wrapper">
					<div class="p_case">
						<h2 class="c_ttl-b">「<span class="is_pttl"><?php the_title(); ?></span>」の<br class="sp-only">症例</h2>
						<div class="p_case__inner">
							<?php
							$cases = $b;
							$i = 0;
							foreach($cases as $cid):
								$be_img = get_field('before_images_before_image_01',$cid);
								$af_img = get_field('after_images_after_image_01',$cid);
								$du_img = get_template_directory_uri().'/img/case_dummy.jpg';
								$pr = get_field('price',$cid);
								$tr = get_field('trouble',$cid);
								$do = get_field('downtime_risk',$cid);
								$i++;
							?>
							
							<?php if($cid): ?>
							<div class="p_case__item">
								<div class="p_case__ttl">CASE <?php echo sprintf("%02d", $i); ?></div>

								<ul class="p_case__img">
									<li><img src="<?php echo ( $be_img ) ? esc_url($be_img['url']) : $du_img; ?>" alt="BEFORE" /><div>BEFORE</div></li>
									<li><img src="<?php echo ( $af_img ) ? esc_url($af_img['url']) : $du_img; ?>" alt="AFTER" /><div>AFTER</div></li>
								</ul>

								<table class="p_case__tbl">
									<tr>
										<th>費用</th>
										<td><?php echo esc_html( $pr ); ?></td>
									</tr>
									<tr>
										<th>施術内容</th>
										<td><?php echo esc_html( $tr ); ?></td>
									</tr>
									<tr>
										<th>ダウンタイム・リスク</th>
										<td><?php echo esc_html( $do ); ?></td>
									</tr>
								</table>
							</div>
							<?php endif; ?>
							<?php endforeach; ?>							
						</div>
					</div>
				</div>
			</section>
			<?php endif; ?>

			<?php if ($c || $d || $e): ?>
			<div class="f_section is_white">
				<?php if ($c): ?>
				<a class="f_anchor" id="flow"></a>
				<section class="f_flow">
					<div class="l_wrapper">
						<h2 class="c_ttl-b">施術の流れ</h2>
						<div class="p_flow">
							<?php echo apply_filters('the_content', $c); ?>
						</div><!-- .p_flow -->
					</div><!-- .l_wrapper -->
				</section><!-- .f_flow -->
				<?php endif; ?>

				<?php if ($d): ?>
				<a class="f_anchor" id="faq"></a>
				<section class="f_faq">
					<div class="l_wrapper">
						<h2 class="c_ttl-b">よくある質問</h2>
						<div class="p_faq">
							<?php echo $d; ?>
						</div><!-- .p_faq -->
					</div><!-- .l_wrapper -->
				</section><!-- .f_faq -->
				<?php endif; ?>

				<?php if ($e): ?>
				<a class="f_anchor" id="charges"></a>
				<section class="f_charges">
					<div class="l_wrapper">
						<h2 class="c_ttl-b">主な料金</h2>
						<div class="p_charges">
							<?php echo get_post_field('post_content', $e); ?>
							<a class="c_btn" href="/price"><span class="c_btn__txt">お支払いについて</span></a>
						</div><!-- .p_charges -->
						
					</div><!-- .l_wrapper -->
				</section><!-- .f_charges -->
				<?php endif; ?>
			</div>
			<?php endif; ?>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
