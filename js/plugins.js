$(document).ready(function(){

	// Troubles Tabs
	$( "#tabs" ).tabs();

	// Header Fixed Position
	$('.f_site__fixed').css({
		'top' : $('.p_header').height() + 30
	});
	$(window).scroll(function() {
		var sc = $(this).scrollTop();
		var scH = 100 + $('.f_site_banner').height();
		var schead = $('.p_header').height();
		var sclogo = $('.p_header__logo .is_top').height();

		if ((sc > sclogo) && (sc < scH)) {
			$('header.p_header.inactive').addClass('off');			
		} else if (sc < sclogo) {
			$('header.p_header.inactive').removeClass('off');
		}

		if (sc > scH) {			
				$('header.p_header').addClass('affix');
		} else {
			$('header.p_header').removeClass('affix');
		}		
		
		if (sc > schead) {
			$('.c_btn-fixed a.is_pagetop img').fadeIn();
		} else {
			$('.c_btn-fixed a.is_pagetop img').fadeOut();
		}	
	});

	$(".c_btn-fixed a.is_pagetop").click(function() {
		$("html, body").animate({scrollTop: 0}, 1000);
	});

	//Main Slider
	$('.fade').slick({
		dots: true,
		infinite: true,
		speed: 300,
		autoplay: true,
		fade: true,
		arrows: false,
		pauseOnHover: false,
		respondTo: 'window',
		responsive: null,
		rows: 1,
		slidesToShow: 1,
		swipe: true,
		cssEase: 'linear'
	});

	// Pick-Up Slick
	$('.p_pickup__slick').slick({
		dots: false,
		arrows: true,
		infinite: true,
		speed: 1000,
		slidesToShow: 1,
		slidesToScroll: 3,
		autoplay: true,
		autoplaySpeed: 3000,
		cssEase: 'ease-in-out',
		variableWidth: true,
		centerMode: true,
		//mobileFirst: true,    
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 3,
					variableWidth: false
				} 
			}
		]
	});
	
	//PC Main Navigation Dropdown
	var checkWidth = $(window).width();
	if (checkWidth > 767) {		
		$(function() {
			$('ul.menu > li').hover(function() {
				$(this).children('.p_dropdown').slideToggle();
			}, function() {
				$(this).children('.p_dropdown').hide();
			});
		});
	} else {	
		$(function(){
			$('ul.menu > li').off( "mouseenter mouseleave" );

			var pickupH = $('.p_pickup__slide .p_pickup__media').height() / 2;
			$('.p_pickup__slick .slick-prev, .p_pickup__slick .slick-next').css({'top': pickupH});
		});
	}

	var main_inner = $('.c_banner__img2').height();
	$('.c_banner-e').css({'height' : main_inner});

});

//Resize windows
$(window).on('resize',function(){
	$('ul.menu > li .p_dropdown').css({'display':'none'});
	$('ul.menu > li').removeClass('active');

	var checkWidth = $(window).width();
	if (checkWidth > 767) {
		$(function() {	
			$('ul.menu > li').off( "mouseenter mouseleave" );
			$('ul.menu > li').hover(function() {
				$(this).children('.p_dropdown').slideToggle();
			}, function() {
				$(this).children('.p_dropdown').hide();
			});
		});
	} else {
		$(function(){	
			$('ul.menu > li').off( "mouseenter mouseleave" );

			var pickupH = $('.p_pickup__slide .p_pickup__media').height() / 2;
			$('.p_pickup__slick .slick-prev, .p_pickup__slick .slick-next').css({'top': pickupH});
		});
	}	

	var main_inner = $('.c_banner__img2').height();
	$('.c_banner-e').css({'height' : main_inner});
});


$(function(){
	// SP Bottom Fixed
	$('.c_btn-fixed .is_blue li .is_blue__ph').click(function(){
		$('.c_btn-fixed .is_gray').slideToggle();
	});

	// SP Menu Toggle
	$('.is_toggle').click(function(){
		$(this).toggleClass('active');
		$('.p_header__nav').toggleClass('open');
		$('.p_header').toggleClass('active');
		$('.p_header').toggleClass('inactive');		
		$('.p_header').removeClass('off');
	});

	//Main Navigation Dropdown
	$('.p_header__nav__list ul.menu li .is_arrow').click(function() {
		$(this).siblings('.p_dropdown').slideToggle();
		$(this).parent().toggleClass('active');	
	});

	//Price List Dropdown
	$('.p_list__tbl__row .p_list__head').click(function() {
		$(this).siblings('.p_list__inner').slideToggle();
		$(this).toggleClass('active');	
	});

	// Smooth Scroll
	var sc = new SmoothScroll('.js_scroll', {
		speed: 500,//スクロールする速さ
		header: '.p_header'//固定ヘッダーがある場合
	});
});

jQuery( function($) {
	jQuery( "#datepicker1,#datepicker2,#datepicker3" ).datepicker({
		minDate: 0,
		yearSuffix:"\u5e74",
		dateFormat: "yy/mm/dd",				
		dayNames:["\u65e5\u66dc\u65e5","\u6708\u66dc\u65e5","\u706b\u66dc\u65e5","\u6c34\u66dc\u65e5","\u6728\u66dc\u65e5","\u91d1\u66dc\u65e5","\u571f\u66dc\u65e5"],
		dayNamesMin:["\u65e5","\u6708","\u706b","\u6c34","\u6728","\u91d1","\u571f"],
		dayNamesShort:["\u65e5\u66dc","\u6708\u66dc","\u706b\u66dc","\u6c34\u66dc","\u6728\u66dc","\u91d1\u66dc","\u571f\u66dc"],
		monthNames:["1\u6708","2\u6708","3\u6708","4\u6708","5\u6708","6\u6708","7\u6708","8\u6708","9\u6708","10\u6708","11\u6708","12\u6708"],
		monthNamesShort:["1\u6708","2\u6708","3\u6708","4\u6708","5\u6708","6\u6708","7\u6708","8\u6708","9\u6708","10\u6708","11\u6708","12\u6708"],
		showMonthAfterYear:"true",
		changeYear:"true",
		changeMonth:"true"	
	});
} );