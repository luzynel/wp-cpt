<?php 

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>
	
	<div id="main_area" class="f_site_main">
		<main>			
			<?php the_content(); ?>
			<section class="f_section is_white">
<div class="l_wrapper">
  <div class="p_list">
    <ul class="p_list__2col">
			<li>
				<ul class="p_list__1col">
					<li>
						<div class="c_ttl-e"><a class="c_btn-b" href="">トップページ</a></div>        
					</li>
					<li>
						<div class="c_ttl-e"><a class="c_btn-b" href="">診療内容</a></div>
						<ul class="p_list__dot">
							<li><a href="">オーロラ（フォトRF）</a></li>
							<li><a href=""> 肝斑MAXトーニング </a></li>
							<li><a href=""> Q-YAGレーザー</a></li>
							<li><a href="">ダーマペン4</a></li>
							<li><a href=""> エレクトロポレーション </a></li>
							<li><a href="">ピーリング（サルチル酸ピーリング・ベビーピール）</a></li>
							<li><a href=""> 内服薬・外用薬 </a></li>
							<li><a href="">PRP皮膚再生</a></li>
							<li><a href="">臍帯血再生因子療法</a></li>
							<li><a href=""> ヒアルロン酸（リデンシティＩ・Ⅱ）（ボリューマXC/クレビエル）</a></li>
							<li><a href="">医療HIFU(ウルトラセルQプラス) </a></li>
							<li><a href="">テノール</a></li>
							<li><a href="">水光注射</a></li>
							<li><a href="">CO2エスプリ（炭酸ガスレーザー）</a></li>
							<li><a href="">CO2フラクショナルレーザー</a></li>
							<li><a href=""> イオン導入 スレッドリフト（シルエットソフト/Gコグリフト/ウルトラVリフト/ビタミンVリフト）</a></li>
							<li><a href="">  二重 BNLSneo</a></li>
							<li><a href="">医療レーザー脱毛（べラックス）</a></li>
							<li><a href="">注射（プラセンタ/グルタチオン/ビタミンＣ） </a></li>
							<li><a href=""> 点滴（高濃度ビタミンＣ） </a></li>
							<li><a href=""> アートメイク 眉・ アイライン  </a></li>
							<li><a href="">  ピアス 耳 </a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li>
				<ul class="p_list__1col">
					<li>
						<div class="c_ttl-e"><a class="c_btn-b" href="">症例一覧</a></div>        
					</li>
					<li>
						<div class="c_ttl-e">クリニック紹介</div>
						<ul class="p_list__dot">
						<li><a href="">当院について</a></li>
						<li><a href=""> 初めての方へ </a></li>
						<li><a href="">ドクター紹介</a></li>
						<li><a href="">川崎駅前スキンコスメクリニック</a></li>
						<li><a href=""> 戸塚駅前スキンコスメクリニック </a></li>
						<li><a href="">二子玉川スキンクリニック</a></li>
						<li><a href=""> 採用情報 </a></li>
						</ul>
					</li>
					<li>
						<div class="c_ttl-e">料金・お支払</div>
						<ul class="p_list__dot">
							<li><a href="">プライスリスト </a></li>
							<li><a href="">お支払いについて</a></li>
						</ul>
					</li>
					<li>
						<div class="c_ttl-e"><a class="c_btn-b" href="">コラム</a></div>        
					</li>
					<li>
						<div class="c_ttl-e"><a class="c_btn-b" href="">お知らせ</a></div>        
					</li>
					<li>
						<div class="c_ttl-e"><a class="c_btn-b" href="">無料相談メール</a></div>        
					</li>
					<li>
						<div class="c_ttl-e"><a class="c_btn-b" href="">無料カウンセリング予約</a></div>        
					</li>
					<li>
						<div class="c_ttl-e"><a class="c_btn-b" href="">プライバシーポリシー</a></div>        
					</li>
					<li>
						<div class="c_ttl-e"><a class="c_btn-b" href="">ご利用規約</a></div>        
					</li>
				</ul>
			</li>
    </ul>
  </div>  
</div>
</section>
			<?php get_template_part( 'inc/contact' ); ?>
		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
