<section class="f_contact">
    <div class="l_wrapper">
        <h2 class="c_ttl c_ttl-b"><span>CONTACT</span>お問い合わせ</h2>
        <div class="p_contact">
            <p class="p_contact__txt">あなたの悩みを直接うかがい、一緒によい方法を考えていきますので、お気軽にご連絡ください。</p>
            <ul class="p_contact__menus">
                <li><a class="c_btn" href="<?php echo get_home_url(); ?>/yoyaku/"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-calendar.svg" alt="calendar" />無料カウンセリング予約</a></li>
                <li><a class="c_btn" href="<?php echo get_home_url(); ?>/sodan/"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-mail.svg" alt="mail" />無料メール相談</a></li>
            </ul>
            <div class="p_contact__info">
                <div class="p_contact__ttl">お電話でのご予約・お問い合わせ<span class="p_contact__subtxt sp-only">電話ご予約受付　10：00～19：00　不定休</span></div>
                
                <ul class="p_contact__inquiries pc-only">
                    <li>
                        <div class="p_contact__name">川崎駅前スキンコスメクリニック<br /><span>KAWASAKI SKIN COSME CLINIC</span></div>
                        <div class="p_contact__tel">
                            <span><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="icon" />0120-63-4112</span>
                        </div>
                        <div class="p_contact__time">診療時間：10：00～19：00　不定休</div>
                    </li>
                    <li>
                        <div class="p_contact__name">戸塚駅前スキンコスメクリニック<br /><span>TOTSUKA SKIN COSME CLINIC</span></div>
                        <div class="p_contact__tel">
                            <span><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="icon" />0120-28-4112</span>
                        </div>
                        <div class="p_contact__time">診療時間：10：00～19：00　不定休</div>
                    </li>
                    <li>
                        <div class="p_contact__name">二子玉川スキンクリニック<br /><span>FUTAKOTAMAGAWA SKIN COSME CLINIC</span></div>
                        <div class="p_contact__tel">
                            <span><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="icon" />0120-71-4112</span>
                        </div>
                        <div class="p_contact__time">診療時間：10：00～19：00　不定休</div>
                    </li>
                </ul>
                <ul class="p_contact__menus is_style2 sp-only">
                    <li><a class="c_btn" href="tel:0120634112"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel-white.svg" alt="telephone" />川崎駅前院</a></li>
                    <li><a class="c_btn" href="tel:0120284112"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel-white.svg" alt="telephone" />戸塚駅前院</a></li>
                    <li><a class="c_btn" href="tel:0120714112"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-tel-white.svg" alt="telephone" />二子玉川院</a></li>
                </ul>
            </div><!-- .p_contact__info -->
        </div><!-- .p_contact -->
    </div><!-- .l_wrapper -->
</section><!-- .f_contact -->