<div class="f_site_banner" id="pageBanner">
  <div class="f_site_banner__wrapper is_inner is_shadow">
    <?php 
      $f_img = get_the_post_thumbnail_url(get_the_ID(),'full');
      $f_img_sp = get_field('sp_featured_image'); 
      if($f_img) {
        echo '<div class="c_banner__img pc-only"><img src="'.$f_img.'" alt="Main Visual"></div>';
        echo '<div class="c_banner__img sp-only"><img src="';
        if($f_img_sp) {
          echo esc_url($f_img_sp['url']);
        }else {
          echo $f_img;
        }
        echo '" alt="Main Visual"></div>';
      }else {
        if (is_post_type_archive('column') || is_singular('column')) {
          echo '<div class="c_banner__img pc-only"><img src="'.get_template_directory_uri().'/img/inner_bnr_column.png" alt="Main Visual"></div><div class="c_banner__img sp-only"><img src="'.get_template_directory_uri().'/img/inner_bnr_column_sp.png" alt="Main Visual"></div>';
        }elseif (is_category('news') || is_singular('post')) {
          echo '<div class="c_banner__img pc-only"><img src="'.get_template_directory_uri().'/img/inner_bnr_news.png" alt="Main Visual"></div><div class="c_banner__img sp-only"><img src="'.get_template_directory_uri().'/img/inner_bnr_news.png" alt="Main Visual"></div>';
        }elseif (is_post_type_archive('case') || is_singular('case')) {
          echo '<div class="c_banner__img pc-only"><img src="'.get_template_directory_uri().'/img/inner_bnr_case.png" alt="Main Visual"></div><div class="c_banner__img sp-only"><img src="'.get_template_directory_uri().'/img/inner_bnr_case.png" alt="Main Visual"></div>';
        }else {
          echo '<div class="c_banner__img"><img src="'.get_template_directory_uri().'/img/bnr_inner_img_default.jpg" alt="Main Visual"></div>';
        }        
      }
    ?>			
    <div class="c_banner__detail">
      <div class="l_wrapper">
        <h1 class="c_banner-b__ttl is_large">
          <?php
            if (is_post_type_archive('column') || is_singular('column')) {
              echo '<span class="is_pttl">ビューティーコラム</span><small>Beauty Column</small>';
            }elseif (is_category('news') || is_singular('post')) {
              echo '<span class="is_pttl">お知らせ</span><small>News & Topics</small>';
            }elseif (is_post_type_archive('case') || is_singular('case')) {
              echo '<span class="is_pttl">症例一覧</span><small>Cases</small>';
            }elseif (is_post_type_archive()) {
              echo '<span class="is_pttl">'.post_type_archive_title().'</span>';
            }else {
              echo '<span class="is_pttl">'.get_the_title().'</span>';
              if (!is_singular('menu')) { 
                echo '<small>'.get_field('sub_title').'</small>'; 
              }
            }
          ?>       
        </h1>
      </div>
    </div>
  </div>
</div><!-- .f_site__banner -->