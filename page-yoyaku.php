<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">					
					<div class="c_content__inner">
						<p>当院のカウンセリングは無料です。院長があなたの悩みを直接うかがい、一緒によい方法を考えていきますので、お気軽にご連絡ください。</p>
						<p>※ご返事に、数日お時間をいただく場合がございます。</p>
						<p>なお、翌日から2日後までのご予約は、お手数ですが各院までご連絡ください。</p>

						<div class="p_faq__box p_contact__box">
              <div class="p_faq__btns">
                <a class="c_btn" href="/sodan"><span class="c_btn__txt">無料メール相談</span></a>
              </div>
              <ul class="p_contact__inquiries p_faq__contact">
                <li><a href="tel:0120634112">
                    <div class="p_contact__name">川崎駅前院</div>
                    <div class="p_contact__tel"><span>0120-63-4112</span></div>
                    <div class="p_contact__time">（診療時間：10：00～19：00　不定休）</div>
                </a></li>
                <li><a href="tel:0120284112">
                    <div class="p_contact__name">戸塚駅前院</div>
                    <div class="p_contact__tel"><span>0120-28-4112</span></div>
                    <div class="p_contact__time">（診療時間：10：00～19：00　不定休）</div>
                </a></li>
                <li><a href="tel:0120714112">
                    <div class="p_contact__name">二子玉川院</div>
                    <div class="p_contact__tel"><span>0120-71-4112</span></div>
                    <div class="p_contact__time">（診療時間：10：00～19：00　不定休）</div>
                </a></li>
              </ul>
              <div class="p_contact__time sp-only">診療時間：10：00～19：00　不定休</div>
            </div>
					</div>
				</div>
			</section>

			<section class="f_section2 is_white">
				<div class="l_wrapper">
					<h2 class="c_ttl-b">無料カウンセリング予約フォーム</h2>
					<div class="p_form">
						<h3 class="c_ttl-c"><span>基本情報</span></h3>
						<div class="p_form__inner">
							<form>
								<div class="p_form__content">
									<p><small><span class="is_red">※</span> 印は入力必須項目です。</small></p>
								</div>
								<table class="p_form__tbl">
									<tr>
										<th>氏名 <span class="is_red">※</span></th>
										<td>
											<input type="text" name="name" class="is_wide" size="60" value="">
										</td>
									</tr>
									<tr>
										<th>フリガナ <span class="is_red">※</span></th>
										<td>
											<input type="text" name="phonetic" class="is_wide" size="60" value="">
										</td>
									</tr>
									<tr>
										<th>性別 <span class="is_red">※</span></th>
										<td>
											<div class="p_form__radio">
												<label>
													<input type="radio" name="gender" value="女性">
													<span>女性</span>
												</label>
												<label>
													<input type="radio" name="gender" value="男性">
													<span>男性</span>
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>年齢 <span class="is_red">※</span></th>
										<td>
											<input type="number" name="age" class="is_wide" size="60" value="" min="1" max="99" placeholder="選択してください">
										</td>
									</tr>
									<tr>
										<th>都道府県名 <span class="is_red">※</span></th>
										<td>
											<input type="text" name="prefecture" class="is_wide" size="60" value="" placeholder="選択してください">
										</td>
									</tr>
									<tr>
										<th>E-Mailアドレス <span class="is_red">※</span></th>
										<td>
											<input type="text" name="email" class="is_wide" size="60" value="">
										</td>
									</tr>
									<tr>
										<th>E-Mailアドレス［確認用］ <span class="is_red">※</span></th>
										<td>
											<input type="text" name="confirm_email" class="is_wide" size="60" value="">
										</td>
									</tr>
									<tr>
										<th>電話番号</th>
										<td>
											<div class="p_form__3fields">
												<input type="text" name="ph_1" class="is_smwide" size="60" value="">
												<span>-</span>
												<input type="text" name="ph_2" class="is_smwide" size="60" value="">
												<span>-</span>
												<input type="text" name="ph_3" class="is_smwide" size="60" value="">
											</div>											
										</td>
									</tr>
									<tr>
										<th>ご希望のクリニック <span class="is_red">※</span></th>
										<td>
											<div class="p_form__radio">
												<label>
													<input type="radio" name="clinic" value="川崎駅前院">
													<span>川崎駅前院</span>
												</label>
												<label>
													<input type="radio" name="clinic" value="戸塚駅前院">
													<span>戸塚駅前院</span>
												</label>
												<label>
													<input type="radio" name="clinic" value="二子玉川院">
													<span>二子玉川院</span>
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>カウンセリング希望日</th>
										<td>
											<div class="p_form__dates">
												<label>第１希望 <span class="is_red">※</span></label>
												<div class="is_dfields">
													<div class="is_calendar">
														<input type="text" name="date_1" id="datepicker1" class="is_date" size="60" value="">
													</div>
													<span>-</span>
													<div class="is_select">
														<select name="counseling_date" class="is_wide">
															<option value="選択してください">選択してください		</option>
															<option value="10：00 - 11：00">10：00 - 11：00		</option>
															<option value="11：00 - 12：00">11：00 - 12：00		</option>
															<option value="12：00 - 13：00">12：00 - 13：00		</option>
														</select>
													</div>
												</div>
											</div>
											<div class="p_form__dates">
												<label>第2希望</label>
												<div class="is_dfields">
													<div class="is_calendar">
														<input type="text" name="date_1" id="datepicker2" class="is_date" size="60" value="">
													</div>
													<span>-</span>
													<div class="is_select">
														<select name="counseling_date" class="is_wide">
															<option value="選択してください">選択してください		</option>
															<option value="10：00 - 11：00">10：00 - 11：00		</option>
															<option value="11：00 - 12：00">11：00 - 12：00		</option>
															<option value="12：00 - 13：00">12：00 - 13：00		</option>
														</select>
													</div>
												</div>
											</div>
											<div class="p_form__dates">
												<label>第3希望</label>
												<div class="is_dfields">
													<div class="is_calendar">
														<input type="text" name="date_1" id="datepicker3" class="is_date" size="60" value="">
													</div>
													<span>-</span>
													<div class="is_select">
														<select name="counseling_date" class="is_wide">
															<option value="選択してください">選択してください		</option>
															<option value="10：00 - 11：00">10：00 - 11：00		</option>
															<option value="11：00 - 12：00">11：00 - 12：00		</option>
															<option value="12：00 - 13：00">12：00 - 13：00		</option>
														</select>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th>相談内容 <span class="is_red">※</span></th>
										<td>
											<input type="text" name="consultation" class="is_wide" size="60" value="" placeholder="選択してください">
										</td>
									</tr>
									<tr>
										<th>その他ご要望がありまし<br>たらご記入ください</th>
										<td>
											<textarea name="inquiry" class="is_wide is_widearea" cols="50" rows="5"></textarea>
										</td>
									</tr>
									<tr>
										<th></th>
										<td>
											<div class="c_btn is_formsubmit"><input type="submit" name="submitConfirm" value="入力内容を確認"></div>
										</td>
									</tr>
								</table>
							</form>
						</div>
					</div>
				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
