<?php
/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

function theme_url_shortcode( $attrs = array (), $content = '' ) {     
    $theme = ( is_child_theme() ? get_stylesheet_directory_uri() : get_template_directory_uri() ); 
    return $theme;     
}
add_shortcode('theme_path', 'theme_url_shortcode' );

// Prevent WP from adding <p> tags on all post types
function disable_wp_auto_p( $content ) {
	remove_filter( 'the_content', 'wpautop' );
	remove_filter( 'the_excerpt', 'wpautop' );
	return $content;
}
add_filter( 'the_content', 'disable_wp_auto_p', 0 );


// Add Thumbnail Support
add_theme_support('post-thumbnails');

function create_post_type() {
	$exampleSupports = [ 
	  'title',  // 記事タイトル
	  'editor',  // 記事本文
	  'thumbnail',  // アイキャッチ画像
	  'revisions'  // リビジョン
	];
  
	//Case 症例一覧
	register_post_type( 'case',
	  array(
		'label' => '症例一覧', 
		'public' => true,
		'has_archive' => true, 
		'menu_position' => 4, 
		'supports' => $exampleSupports
	  )
	);

	//Column コラム
	register_post_type( 'column',
	  array(
		'label' => 'ビューティーコラム', 
		'public' => true,  
		'has_archive' => true, 
		'menu_position' => 5, 
		'supports' => $exampleSupports
	  )
	);

	//Menu 診療内容
	register_post_type( 'menu',
	  array(
		'label' => '診療内容', 
		'public' => true,  
		'has_archive' => true, 
		'menu_position' => 6, 
		'supports' => $exampleSupports
	  )
	);

	//Price プライスリスト
	register_post_type( 'pricelist',
	  array(
		'label' => 'プライスリスト',
		'has_archive' => false, 
		'menu_position' => 7, 
		'supports' => $exampleSupports,
		'public' => true,
		'exclude_from_search' => true,
		'show_in_admin_bar'   => false,
		'show_in_nav_menus'   => false,
		'publicly_queryable'  => false,
		'query_var'           => false
	  )
	);
}
add_action( 'init', 'create_post_type' ); 

//query posts function
function custom_posttype($catname,$posttype,$postsnum) {
	global $w_query;
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$w_query = new WP_Query( 
		array(
				'category_name' => $catname,
				'post_type'     => $posttype,
				'paged'         => $paged,
				'order'         => 'DESC',				
				'post_status'   => 'publish',
				'posts_per_page' => $postsnum
		)
	);
}
//pagination issue fix change posts per page here
function case_posts( $query ) {  
	if( $query->is_main_query() && !$query->is_feed() && !is_admin() && is_post_type_archive() ) {  
			$query->set( 'posts_per_page', '1' );  
	}  
}
add_action('pre_get_posts','case_posts'); 

//posts pagination
function posts_pagination() {
	global $w_query;
	$big = 999999999; // need an unlikely integer
	echo '<div class="c_postnav">';
	echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'prev_text' => false,
		'next_text' => false,
		'current' => max( 1, get_query_var('paged') ),
		'total' => $w_query->max_num_pages,
		'type' => 'list'
		) );
	echo '</div>';
}

//News pagination issue fix
function custom_pre_get_posts( $query ) {  
	if( $query->is_main_query() && !$query->is_feed() && !is_admin() && is_category()) {  
			$query->set( 'paged', str_replace( '/', '', get_query_var( 'page' ) ) );  
	}  
}
add_action('pre_get_posts','custom_pre_get_posts'); 
function custom_request($query_string ) { 
	if( isset( $query_string['page'] ) ) { 
			if( ''!=$query_string['page'] ) { 
					if( isset( $query_string['name'] ) ) { unset( $query_string['name'] ); } } } return $query_string; 
} 
add_filter('request', 'custom_request');
//end News pagination issue fix