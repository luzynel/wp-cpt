<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">
					<div class="p_detail">
						<div class="p_detail__item">
							<h3 class="c_ttl-c"><span>当院の料金システム</span></h3>
							<div class="p_detail__content">
								<p>当院は、<a href="/yoyaku">初回のカウンセリング無料です。</a>料金は施術料金のみとなります。<a href="/price/list">料金の詳細はこちらをご確認ください。</a></p>
								<p>&nbsp;</p>
								<p>※但し、料金はあくまでも目安です。お客様のご相談内容で変わって参りますので、料金をお見積もりするためにも、一度<a href="/sodan">無料カウンセリングにお越しください</a></p>
							</div>
						</div>
						<div class="p_detail__item">
							<h3 class="c_ttl-c"><span>お支払いについて</span></h3>
							<div class="p_detail__content">
								<div class="c_flex">
									<div class="p_faq__logo p_detail__logo">
										<div class="p_detail__logo-ttl">現金でのお支払い</div>
										<ul>
											<li><a href=""><img src="http://localhost/lrendon/Skin_Cosme/wordpress/wp-content/themes/skincosme/img/icon_cash.svg" alt="CASH"></a></li>
										</ul>
									</div>
									<div class="p_faq__logo p_detail__logo">
										<div class="p_detail__logo-ttl">クレジットカードでのお支払い</div>
										<ul>
											<li><a href=""><img src="http://localhost/lrendon/Skin_Cosme/wordpress/wp-content/themes/skincosme/img/logo_bank-01.jpg" alt="VISA"></a></li>
											<li><a href=""><img src="http://localhost/lrendon/Skin_Cosme/wordpress/wp-content/themes/skincosme/img/logo_bank-02.jpg" alt="マスター"></a></li>
											<li><a href=""><img src="http://localhost/lrendon/Skin_Cosme/wordpress/wp-content/themes/skincosme/img/logo_bank-03.jpg" alt="JCB"></a></li>
											<li><a href=""><img src="http://localhost/lrendon/Skin_Cosme/wordpress/wp-content/themes/skincosme/img/logo_bank-04.jpg" alt="アメリカン・エキスプレス"></a></li>
											<li><a href=""><img src="http://localhost/lrendon/Skin_Cosme/wordpress/wp-content/themes/skincosme/img/logo_bank-05.jpg" alt="ダイナース"></a></li>
											<li><a href=""><img src="http://localhost/lrendon/Skin_Cosme/wordpress/wp-content/themes/skincosme/img/logo_bank-06.jpg" alt="銀聯"></a></li>
											<li><a href=""><img src="http://localhost/lrendon/Skin_Cosme/wordpress/wp-content/themes/skincosme/img/logo_bank-07.jpg" alt="ディスカバー"></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="p_detail__item">
							<h3 class="c_ttl-c"><span>ポイント制について</span></h3>
							<div class="p_detail__content">
								<div class="p_detail__boxed-txt"><span>当院ではポイントカードを発行しております。</span></div>
								<div class="p_list__order is_order2">
									<ul>
										<li><span>1.</span>還元率･･･現金10,000円につき300ポイント、カードﾞ10,000円につき100ポイント</li>
										<li><span>2.</span>換金率･･･1ポイントで1円換算</li>
										<li><span>3.</span>有効期限･･･最終来院日より2年間有効</li>
										<li><span>4.</span>使用単位･･･1,000ポイント単位でご利用できます</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="p_detail__item">
							<h3 class="c_ttl-c"><span>お友達紹介のご案内</span></h3>
							<div class="p_detail__content">
								<div class="p_detail__boxed-txt"><span>大切なお友達にＫＳＣＣを紹介して、一緒にキレイになりませんか。</span></div>
								<p>お友達が初めて当院で3万円以上の施術をお受けいただくと、<br> 
									あなたとお友達に5000ポイントをプレゼント！！ <br>
									<a href="">★5000ポイントは5000円分</a>に相当します。 </p>
								<p>当院での診療費用やDrコスメ購入の際、ご利用いただけます。</p>
								<div class="p_detail__box">
									<div>
										<p>施術内容、金額等のご相談がございましたら、<br class="pc-only">右のボタンから、お問い合わせをお願いいたします。</p>
										<div class="p_detail__box-phone">
											<img src="http://localhost/lrendon/Skin_Cosme/wordpress/wp-content/themes/skincosme/img/icon_mobile.svg" alt="">
											お電話やメールでも、<br>ご質問をお受けいたします。
										</div>
									</div>
									<div>
										<a class="c_btn" href="/sodan"><span class="c_btn__txt">無料メール相談</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
