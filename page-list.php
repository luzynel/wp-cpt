<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">
					<div class="c_content__inner">
						<?php the_content(); ?>

						<div class="p_list__tbl">
							<?php
							$args = array(
								'post_type' => 'pricelist',
								'numberposts' => -1,
								'order' => 'DESC'
							);
							$plist = new WP_Query($args);
							while ($plist->have_posts()) : $plist->the_post(); ?>
								<div id="<?php echo get_the_ID(); ?>" class="p_list__tbl__row">
									<h3 class="c_ttl-c p_list__head"><span><?php the_title(); ?></span></h3>
									<div class="p_list__inner">
										<?php the_content(); ?>
									</div>
								</div>
							<?php endwhile; wp_reset_postdata();  ?>
						</div>
					</div>
				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
